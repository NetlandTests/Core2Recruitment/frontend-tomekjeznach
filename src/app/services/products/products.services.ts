import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { RestService } from '../auth/rest.serivce';
import { Product } from '../models/ProductDTO.model';

@Injectable()
export class ProductsService {

  constructor(private http: HttpClient, private rest: RestService) {

  }

  getProducts(): any {
    return this.rest.get<Product[]>('Products');
  }
}

