import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { apiURL } from '../../config/config.service';
import * as _ from 'lodash';

@Injectable()
export class RestService {

  constructor(private http: HttpClient) {

  }

  get<T1>(action: string): Observable<T1> {
    return this.http.get<T1>(this.getFullEndpointUrl(action));
  }

  post(action: string, body, sendAsFormData = false): Observable<any> {
    let options;
    if (sendAsFormData) {
      options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };
      body = this.getHttpParamsBody(body);
    }
    return this.http.post(this.getFullEndpointUrl(action), body, options);
  }

  private getFullEndpointUrl(action: string) {
    return `${apiURL}${action}`;
  }

  private getHttpParamsBody(body): HttpParams {
    let httpParamsBody = new HttpParams();
    _.forEach(body, (value, key) => {
      httpParamsBody = httpParamsBody.append(key, value);
    });
    return httpParamsBody;
  }

}
