import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private jwtHelperService: JwtHelperService) {

  }

  canActivate(): boolean {
    if (!this.jwtHelperService.tokenGetter()) {
      this.router.navigate(['/login']);
      return false;
    } else {
      return true;
    }
  }
}
