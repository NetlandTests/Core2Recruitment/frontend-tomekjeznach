import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class NoAuthGuard implements CanActivate {

  constructor(private router: Router, private jwtHelperService: JwtHelperService) {

  }

  canActivate(): boolean {
    if (this.jwtHelperService.tokenGetter() && !this.jwtHelperService.isTokenExpired()) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
