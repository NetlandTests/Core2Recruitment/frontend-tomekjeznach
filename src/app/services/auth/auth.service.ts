import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Login } from '../models/LoginDTO.model';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.serivce';
import { map } from 'rxjs/operators';
import { Token } from '../models/tokenDTO.model';


@Injectable()
export class AuthService {
  static accessToken = 'access_token';

  constructor(private jwtHelper: JwtHelperService, private rest: RestService, private router: Router) { }

  login(loginData: Login): Observable<any> {
    return this.rest.post('Jwt', loginData, true)
      .pipe(map((result: Token) => {
        localStorage.setItem(AuthService.accessToken, result.access_token);
        return result;
      }));
  }

  logout() {
    localStorage.removeItem(AuthService.accessToken);
    this.router.navigate(['/login']);
    return null;
  }


}
