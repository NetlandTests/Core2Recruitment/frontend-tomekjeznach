import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/auth/rest.serivce';
import { Product } from '../../services/models/ProductDTO.model';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { ProductsService } from '../../services/products/products.services';
import swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[];

  constructor(private rest: RestService, private auth: AuthService, private router: Router, private productsService: ProductsService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.productsService.getProducts().subscribe((res) => {
      this.products = res;
    }, error => {
      swal({
        title: 'Wystąpił błąd po stronie serwera.',
        text: '',
        type: 'error',
        timer: 1000,
        showConfirmButton: false
      });
      this.router.navigate(['/login']);

    });
  }

  logout() {
    this.auth.logout();
  }
}
