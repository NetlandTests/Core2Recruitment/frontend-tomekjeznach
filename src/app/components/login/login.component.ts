import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { RestService } from '../../services/auth/rest.serivce';
import { Router } from '@angular/router';
import { FormsModule, FormGroup } from '@angular/forms';

import swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public errorText;

  constructor(private auth: AuthService, private rest: RestService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(value) {
    this.auth.login({
      UserName: value.login,
      Password: value.password
    }).subscribe((result) => {
      swal({
        title: 'Logowanie powiodło się',
        text: '',
        type: 'success',
        timer: 1000,
        showConfirmButton: false
        });
        this.router.navigate(['/products']);
    }, (error) => {
      swal({
        title: 'Logowanie nie powiodło się',
        text: 'Podałeś złe dane',
        type: 'error',
        timer: 2000,
        showConfirmButton: false
        });
    });
  }

}
