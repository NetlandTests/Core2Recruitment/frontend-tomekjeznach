import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './services/auth/auth.service';
import { AuthGuard } from './services/auth/auth.guard';
import { ProductsService } from './services/products/products.services';
import { uri } from './config/config.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ProductsComponent } from './components/products/products.component';
import { Routes, RouterModule } from '@angular/router';
import { appRoutes } from './app.routing';
import { RestService } from './services/auth/rest.serivce';
import { NoAuthGuard } from './services/auth/noauth.guard';

const ROUTES: Routes = appRoutes;

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem(AuthService.accessToken);
        },
        whitelistedDomains: [uri],
      }
    })
  ],
  providers: [
    AuthService,
    AuthGuard,
    RestService,
    NoAuthGuard,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
